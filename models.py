# -*- coding: utf-8 -*-

from openerp import models, fields, api

class dmt_game(models.Model):
    _name = 'dmt.game'
    
    name = fields.Char(string='Game')
    init_date = fields.Date(string='Init date')
    description = fields.Text(string='Description')
    currency = fields.Char(string='Game currency')
    game_selected = fields.Boolean(string='Selected')

    player_ids = fields.One2many('dmt.player', 'game_id', string='Players')
    item_ids = fields.One2many('dmt.item', 'game_id', string='Items')

    #only for UI changes on game_selected
    @api.onchange('game_selected')
    def _onchange_selected(self):
        all_games = self.env['dmt.game'].search([['id','!=', -1]])
        for _game in all_games:
            _game.write({'game_selected' : False})

    @api.one
    def action_selection(self):
        new_state = not self.game_selected
        all_games = self.env['dmt.game'].search([['id','!=', -1]])
        for _game in all_games:
            _game.write({'game_selected' : False})
        self.game_selected = new_state
        

class dmt_player(models.Model):
    _name = 'dmt.player'

    is_dm = fields.Boolean(string='Dungeon master', default=False)
    character_name = fields.Char(string='Name')
    character_class = fields.Char(string='Class')
    character_level = fields.Integer(string='Level', default=1)
    character_exp = fields.Integer(string='Exp points', default=0)
    character_sheet = fields.Text(string='Sheet')
    character_bio = fields.Text(string='Biography', default='No bio yet')
    character_earnings = fields.Float(string='Earnings', default=0, digits=(10,2))
    character_item_earnings = fields.Float(compute='_compute_item_earnings', string='Item earnings')
    character_total_earnings = fields.Float(compute='_compute_total_earnings', string='Total earnings')

    game_id = fields.Many2one('dmt.game', string='Game')
    item_ids = fields.One2many('dmt.item', 'item_owner_id', string='Item list')

    name = fields.Char(related='character_name')
    game_currency = fields.Char(related='game_id.currency', string='Currency')
    game_active = fields.Boolean(related='game_id.game_selected')

    @api.one
    def _compute_item_earnings(self):
        item_earnings = 0
        all_items = self.item_ids
        if all_items:
            item_earnings = reduce(lambda x,y: x+y, map(lambda x: x.item_value, all_items))
        else:
            item_earnings = 0
        self.character_item_earnings = item_earnings

    @api.one
    def _compute_total_earnings(self):
        self.character_total_earnings = self.character_earnings + self.character_item_earnings

    

class dmt_item(models.Model):
    _name = 'dmt.item'

    item_name = fields.Char(string='Item name')
    item_description = fields.Text(string='Item description')
    item_owner_id = fields.Many2one('dmt.player', string='Owner')
    item_value = fields.Float(string='Item value', default=0)

    game_id = fields.Many2one(related='item_owner_id.game_id', store=True)

    name = fields.Char(related='item_name')
    currency = fields.Char(related='game_id.currency', string='Currency')
    game_active = fields.Boolean(related='item_owner_id.game_active')
